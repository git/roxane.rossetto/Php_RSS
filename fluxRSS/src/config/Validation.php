<?php
namespace config;

use http\Exception\InvalidArgumentException;

class Validation
{
    public static function val_action($action)
    {
        if (!isset($action)) {
            throw new \Exception('pas d\'action');
            //on pourrait aussi utiliser
            $action = $_GET['action'] ?? '';
            // This is equivalent to:
            //$action =  if (isset($_GET['action'])) $action=$_GET['action']  else $action='no';
        }
    }

    public static function val_form(string &$nom, string &$age, &$dVueEreur)
    {
        if (!isset($nom) || $nom == '') {
            $dVueEreur[] = 'pas de nom';
            $nom         = '';
        }

        if ( strlen(htmlspecialchars($nom, ENT_QUOTES)) != strlen($nom) ) {
            $dVueEreur[] = "testative d'injection de code (attaque sécurité)";
            $nom         = '';
        }

        if (!isset($age) || $age == '' || !filter_var($age, FILTER_VALIDATE_INT)) {
            $dVueEreur[] = "pas d'age ";
            $age         = 0;
        }
    }
    public static function validationLogin(string &$username)
    {
        $username = trim($username);
        if (!isset($username) || !filter_var($username, FILTER_SANITIZE_STRING) || !filter_var($username, FILTER_FLAG_EMPTY_STRING_NULL)){
            return false;
        }
        return true;
    }
    public static function validationMdp(string &$mdp)
    {
        $mdp = trim($mdp);
        if (!isset($mdp) || !filter_var($mdp, FILTER_SANITIZE_STRING) || !filter_var($mdp, FILTER_FLAG_EMPTY_STRING_NULL)){
            return false;
        }
        return true;
    }
    public static function ValidationFlux(string &$flux)
    {
        if (!isset($flux) || !filter_var($flux, FILTER_SANITIZE_URL) || !filter_var($flux, FILTER_VALIDATE_URL)){
            return false;
        }
        return true;
    }
}
