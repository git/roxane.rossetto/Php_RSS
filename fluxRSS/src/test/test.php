<?php
namespace test;
use DAL\ArticleGateway;
use DAL\Connection;
use DAL\FluxGateway;
use model\Parser;

require '../../vendor/autoload.php';

$gwArt = new ArticleGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto', 'rorossetto', 'tpphp'));
$gwFl = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto', 'rorossetto', 'tpphp'));
$pars = new Parser( $gwFl,$gwArt);
$pars->addAllArticles();
var_dump($pars);