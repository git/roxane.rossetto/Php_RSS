<?php

namespace DAL;

use Exception;
use PDO;
use Twig\Error\Error;

class Connection extends PDO
{
    private $stmt; // pas typé, car peut être faux ou statement

    /**
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @throws Exception
     */
    public function __construct(string $dsn, string $username, string $password)
    {
        try{
            parent::__construct($dsn, $username, $password);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (\PDOException $e){
            throw new Exception("PDO error con");
        }
        catch (Error $e){
            throw new Error("Error PDO");
        }
    }

    /**
     * @param string $query to execute
     * @param array $parameters to bind
     * @return bool Returns `true` on success, `false` otherwise
     * @throws Exception
     */
    public function executeQuery(string $query, array $parameters = []): bool
    {
        try{
            $this->stmt = parent::prepare($query);
            foreach ($parameters as $name => $value) {
                $this->stmt->bindValue($name, $value[0], $value[1]);
            }
            if($this->stmt->execute()){
                return true;
            } else {
                $error = $this->stmt->errorInfo();
                throw new Exception("PDO error: ".$error[2]);
            }

        }catch (\PDOException $e){
            throw new Exception("PDO error: ".$e->getMessage());
        }catch (\Error $r){
            throw new Error("executionQuery not possible : ".$r->getMessage());
        }
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->stmt->fetchall();
    }
}