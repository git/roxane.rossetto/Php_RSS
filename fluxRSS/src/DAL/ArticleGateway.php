<?php
namespace DAL;

use Exception;
use metier\Article;
use metier\Flux;
use PDO;
class ArticleGateway
{
    private $con;

    /**
     * @param $con
     */
    public function __construct($con)
    {
        $this->con = $con;
    }

    /**
     * @throws Exception
     */
    public function getAllArticles():array
    {
        $query = 'SELECT * FROM Article ORDER BY datePub DESC;';
        $this->con->executeQuery($query, array());
        return $this->con->getResults();
    }

    /**
     * @throws Exception
     */
    public function addArticle(Article $article){
        $query = "INSERT INTO Article VALUES (:id,:title,STR_TO_DATE(:datePub, '%d/%m/%y %H:%i'),:description,:guid,:link,:mediaContent,:provenance);";
        $this->con->executeQuery($query, array(':id' => array($article->getId(), PDO::PARAM_STR),
            ':title' => array($article->getTitle(), PDO::PARAM_STR),
            ':datePub' => array($article->getDate(), PDO::PARAM_STR),
            ':description' => array($article->getDescription(), PDO::PARAM_STR),
            ':guid' => array($article->getGuid(), PDO::PARAM_STR),
            ':link' => array($article->getLink(), PDO::PARAM_STR),
            ':mediaContent' => array($article->getMediaContent(), PDO::PARAM_STR),
            ':provenance' => array($article->getProvenance(), PDO::PARAM_INT)));
    }

    public function removeAllArticleForParser(){
            $query = 'DELETE FROM Article;';
            $this->con->executeQuery($query);
    }

    public function removeAllArticleFromFlux(int $idFlux){
        try {
            $query = 'DELETE FROM Article WHERE Provenance = :idFlux;';
            $this->con->executeQuery($query, array(':idFlux' => array($idFlux, PDO::PARAM_INT)));
        }catch(\PDOException $p){
            throw new Exception("Data of flux is not delete.");
        }
    }

    public function findArticleByFlux(int $flux){
        $query = 'SELECT * FROM Article WHERE provenance = :flux;';
        $this->con->executeQuery($query, array(':flux' => array($flux, PDO::PARAM_INT)));
        return $this->con->getResults();
    }
}