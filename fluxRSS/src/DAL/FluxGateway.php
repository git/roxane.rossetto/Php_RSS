<?php

namespace DAL;

use metier\Flux;
use PDO;

class FluxGateway
{
    private Connection $con;

    public function __construct(Connection $con){
        $this->con = $con;
    }

    public function addFlux(Flux $flux)
    {
        $this->addFluxBySrc($flux->getFlux());
    }

    public function addFluxBySrc(string $flux)
    {
        $query = 'INSERT INTO Flux VALUES (null,:flux);';
        $this->con->executeQuery($query, array(':flux' => array($flux, PDO::PARAM_STR)));
    }

    public function removeFlux(Flux $flux){
        $this->removeFluxBySrc($flux->getFlux());
    }

    public function removeFluxBySrc(string $flux){
        $query = 'DELETE FROM Flux WHERE flux = :flux;';
        $this->con->executeQuery($query, array(':flux' => array($flux, PDO::PARAM_STR)));
    }

    public function removeFluxById(int $id){
        $query = 'DELETE FROM Flux WHERE id = :id;';
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
    }

    public function findAllFlux(){
        $query = 'SELECT * FROM Flux;';
        $this->con->executeQuery($query);
        return $this->con->getResults();
    }

    public function findFlux(Flux $flux){
        return $this->findFluxBySrc($flux->getFlux());
    }

    public function findFluxBySrc(string $flux){
        $query = 'SELECT * FROM Flux WHERE flux = :flux;';
        $this->con->executeQuery($query, array(':flux' => array($flux, PDO::PARAM_STR)));
        return $this->con->getResults();
    }
}