<?php

namespace DAL;

use PDO;

class AdminGateway
{
    private $con;

    /**
     * @param $con
     */
    public function __construct($con)
    {
        $this->con = $con;
    }

    public function login(string $login):array
    {
        $query = 'SELECT password,mail FROM Admin WHERE name = :login;';
        $this->con->executeQuery($query, array(':login' => array($login, PDO::PARAM_STR)));
        return $this->con->getResults();
    }
}