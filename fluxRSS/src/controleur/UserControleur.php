<?php
namespace controleur;

use model\AdminModel;
use model\ArticleModel;

class UserControleur
{
    public function init()
    {
        global $twig; // nécessaire pour utiliser variables globales
        //debut

        //on initialise un tableau d'erreur
        $dVueEreur = [];

        try {
            $action = $_REQUEST['action'] ?? null;

            switch($action) {
                //pas d'action, on réinitialise 1er appel
                case 'listArticle':
                case null:
                    $this->listArticle();
                    break;

                case 'connection':
                    $this->connection();;
                    break;

                case 'deconnection':
                    $this->deconnection();
                    break;
                case 'validationFormulaire':
                    $this->ValidationFormulaire($dVueEreur);
                    break;

                case 'changeNbArticle':
                    $this->changeNbArticle();
                    break;

                //mauvaise action
                default:
                    $dVueEreur[] = "Erreur d'appel php";
                    echo $twig->render('erreur.html', ['dVueErreur'=>$dVueEreur, 'isAdmin' => AdminModel::isAdmin()]);
                    break;
            }
        } catch (\PDOException $e) {
            //si erreur BD, pas le cas ici
            $dVueEreur[] = 'Erreur PDO : ' . $e->getMessage();
            echo $twig->render('erreur.html', ['dVueEreur' => $dVueEreur]);
        } catch (\Exception $e2) {
            $dVueEreur[] = 'Erreur : ' . $e2->getMessage();
            echo $twig->render('erreur.html', ['dVueEreur' => $dVueEreur]);
        }

        //fin
        exit(0);
    }//fin constructeur

    public function listArticle()
    {
        global $twig;
        $articleModel = new ArticleModel();
        $nbArticle = isset($_SESSION['nbArticle']) ? intval($_SESSION['nbArticle']) : 5;
        $allArticles = $articleModel->getArticles();
        $articles = array_slice($allArticles, 0, $nbArticle);
        $dVue = [
            'data' => $articles
        ];
        echo $twig->render('listArticle.html', [
            'dVue' => $dVue,
            'isAdmin' => AdminModel::isAdmin()
        ]);
    }

    public function changeNbArticle()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['nbArticle'])) {
            $_SESSION['nbArticle'] = $_POST['nbArticle'];
            unset($_POST['action']);
        }
        $this->init();
    }

    /**
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig\Error\LoaderError
     */
    public function connection(){
        global $twig; // nécessaire pour utiliser variables globales
        if (AdminModel::isAdmin()) {
            $this->listArticle();
        }
        else {
            echo $twig->render('Connection.html');
            if (isset($_POST['username']) && isset($_POST['password'])) {
                $this->login();
            }
        }
    }

    public function deconnection(){
        AdminModel::deconnection();
        $this->listArticle();
    }

    /**
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig\Error\LoaderError
     * @throws \Exception
     */
    public function login(){
        $username = $_POST['username'];
        $password = $_POST['password'];

        $adminModel = new AdminModel();
        $admin = $adminModel->connection($username, $password);
        if ($admin != null) {
            $this->listArticle();
        }
        else{
            $this->connection();
        }
    }

    public function ValidationFormulaire(array $dVueEreur)
    {
        global $twig; // nécessaire pour utiliser variables globales

        //si exception, ca remonte !!!
        $nom = $_POST['txtNom']; // txtNom = nom du champ texte dans le formulaire
        $age = $_POST['txtAge'];
        \config\Validation::val_form($nom, $age, $dVueEreur);

        /*
        $model = new \metier\Simplemodel();
        $data  = $model->get_data();
        */
        
        $dVue = [
            'nom'  => $nom,
            'age'  => $age,
            //'data' => $data,
        ];

        echo $twig->render('Connection.html', ['dVue' => $dVue, 'dVueEreur' => $dVueEreur]);
    }
}//fin class
