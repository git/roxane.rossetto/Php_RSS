<?php

namespace controleur;

use config\Validation;
use DAL\ArticleGateway;
use DAL\Connection;
use DAL\FluxGateway;
use http\Exception;
use http\Url;
use metier\Flux;
use model\AdminModel;
use model\ArticleModel;
use model\FluxModel;

class AdminControleur
{
    public function init(){
        global $twig; // nécessaire pour utiliser variables globales
        //debut

        //on initialise un tableau d'erreur
        $dVueEreur = [];

        try {
            $action = $_REQUEST['action'] ?? null;

            switch($action) {
                //pas d'action, on réinitialise 1er appel
                case 'listArticle':
                case null:
                    $this->listArticle();
                    break;

                case 'connection':
                    $this->connection();;
                    break;

                case 'deleteFlux':
                    $this->deleteFlux();;
                    break;

                case 'validationFormulaire':
                    $this->ValidationFormulaire($dVueEreur);
                    break;

                case 'listFlux':
                    $this->listFlux();
                    break;

                case 'ajoutFlux':
                    $this->ajoutFlux();
                    break;

                case 'changeNbArticle':
                    $this->changeNbArticle();
                    break;

                //mauvaise action
                default:
                    $dVueEreur[] = "Erreur d'appel php";
                    echo $twig->render('erreur.html', ['dVueErreur'=>$dVueEreur,'isAdmin' => (AdminModel::isAdmin())]);
                    break;
            }
        } catch (\PDOException $e) {
            //si erreur BD, pas le cas ici
            $dVueEreur[] = 'Erreur PDO : ' . $e->getMessage();
            echo $twig->render('erreur.html', ['dVueEreur' => $dVueEreur]);
        } catch (\Exception $e2) {
            $dVueEreur[] = 'Erreur : ' . $e2->getMessage();
            echo $twig->render('erreur.html', ['dVueEreur' => $dVueEreur]);
        }

        //fin
        exit(0);
    }

    public function listArticle()
    {
        global $twig;
        $articleModel = new ArticleModel();
        $nbArticle = isset($_SESSION['nbArticleAdmin']) ? intval($_SESSION['nbArticleAdmin']) : 5;
        $allArticles = $articleModel->getArticles();
        $articles = array_slice($allArticles, 0, $nbArticle);
        if (AdminModel::isAdmin()) {
            $dVue = [
                'data' => $articles
            ];
            echo $twig->render('listArticleAdmin.html', [
                'dVue' => $dVue,
                'isAdmin' => AdminModel::isAdmin()
            ]);
        }
        else {
            $this->connection();
        }
    }

    public function listFlux(){
        global $twig;
        $fluxModel = new FluxModel();
        if (AdminModel::isAdmin()) {
            $dVue = [
                'data' => $fluxModel->findAllFlux()
            ];
            echo $twig->render('listFlux.html', [
                'dVue' => $dVue,
                'isAdmin' => AdminModel::isAdmin()
            ]);
        }
        else {
            $this->connection();
        }
    }

    public function connection(){
        global $twig; // nécessaire pour utiliser variables globales

        $renderTemplate = true;
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['username'])){
            $_REQUEST['action'] = null;
            $this->login();
            $renderTemplate = false;
        }
        if($renderTemplate) {
            echo $twig->render('Connection.html');
        }
    }

    public function deleteFlux(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['flux'])){
            $articleModel = new ArticleModel();
            $fluxModel = new FluxModel();
            $articleModel->removeArticleIdFlux(intval($_POST['flux']));
            $fluxModel->removeFluxById(intval($_POST['flux']));
            $_REQUEST['action'] = 'listFlux';
            $this->init();
        }
        else{
            $_REQUEST['action'] = 'listFlux';
            $this->init();
        }
    }

    public function ajoutFlux(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['fluxAdd'])){
            $fluxModel = new FluxModel();
            if(Validation::ValidationFlux($_POST['fluxAdd'])){
                $fluxModel->addFluxBySrc($_POST['fluxAdd']);
            }
            $_REQUEST['action'] = 'listFlux';
            unset($_POST['fluxAdd']);
            $this->init();
        }
        else{
            $_REQUEST['action'] = 'listFlux';
            $this->init();
        }
    }

    public function login(){
        $username = $_POST['username'];
        $password = $_POST['password'];

        $adminModel = new AdminModel();
        $admin = $adminModel->connection($username, $password);
        if($admin != null) {
            $this->init();
        }
        else{
            unset($_POST['username']);
            unset($_POST['password']);
            $this->connection();
        }
    }

    public function changeNbArticle()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['nbArticleAdmin'])) {
            $_SESSION['nbArticleAdmin'] = $_POST['nbArticleAdmin'];
            unset($_POST['action']);
        }
        $this->init();
    }
}