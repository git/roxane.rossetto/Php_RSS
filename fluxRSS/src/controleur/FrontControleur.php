<?php

namespace controleur;

use model\AdminModel;
use vendor\AltoRouter;
use controleur\UserControleur;
use controleur\AdminControleur;
require 'AltoRouter.php';

class FrontControleur
{
    public function __construct(){
        $routeur = new AltoRouter();



        global $twig;
        $router = new AltoRouter();
        $router->setBasePath('~rorossetto/Php_RSS/fluxRSS/');
        $router->map('GET', '/', 'UserControleur');
        $router->map('GET|POST', '/[a:action]?', 'UserControleur');
        $router->map('GET|POST','/user/[a:action]?','UserControleur');
        $router->map('GET|POST','/admin/[a:action]?','AdminControleur');

        $match = $router->match();
        if (!$match) {
            $dVueEreur[] = "Page doesn't exist";
            echo $twig->render('erreur.html', ['dVueEreur' => $dVueEreur]);
        }
        else {
            session_start();
            $controller=$match['target'] ?? null;
            $action=$match['params']['action'] ?? "init";
            try {
                if($controller == "AdminControleur"){
                    if (!AdminModel::isAdmin()){
                        $action = "connection";
                    }
                }
                if($action == 'deconnection'){
                    AdminModel::deconnection();
                }
                $controller = '\\controleur\\' . $controller;
                $controller = new $controller;
                if (is_callable(array($controller, $action))) {
                    call_user_func_array(array($controller, $action),
                        array($match['params']));
                }
            }
            catch (Error $error){
                $dVueEreur[] = "Controller doesn't exist";
                echo $twig->render('erreur.html', ['dVueEreur' => $dVueEreur]);
            }
        }
    }


}