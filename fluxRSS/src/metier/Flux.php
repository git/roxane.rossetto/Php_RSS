<?php

namespace metier;

class Flux
{
    private ?int $id;
    private string $flux;

    public function __construct(?int $id, string $flux){
        $this->id =$id;
        $this->flux = $flux;
    }
    /**
     * @return string
     */
    public function getFlux(): string
    {
        return $this->flux;
    }

    /**
     * @param string $flux
     */
    public function setFlux(string $flux): void
    {
        $this->flux = $flux;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}