<?php

namespace metier;

use DateTime;

class Article
{
    private int $id;
    private string $title;
    private string $date;
    private string $description;
    private string $guid;
    private string $link;
    private string $mediaContent;
    private int $provenance;

    /**
     * @param int $id
     * @param string $title
     * @param string $date
     * @param string $description
     * @param string $guid
     * @param string $link
     * @param string $mediaContent
     * @param int $provenance
     */
    public function __construct(
        ?int $id,
        string $title,
        string $date,
        string $description,
        string $guid,
        string $link,
        string $mediaContent,
        int $provenance)
    {
        $this->id = $id;
        $this->title = $title;
        $this->date = $date;
        $this->description = $description;
        $this->guid = $guid;
        $this->link = $link;
        $this->mediaContent = $mediaContent;
        $this->provenance = $provenance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): void
    {
        $this->guid = $guid;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getMediaContent(): string
    {
        return $this->mediaContent;
    }

    public function setMediaContent(string $mediaContent): void
    {
        $this->mediaContent = $mediaContent;
    }

    public function getProvenance(): int
    {
        return $this->provenance;
    }

    public function setProvenance(int $provenance): void
    {
        $this->provenance = $provenance;
    }

    public function __toString(): string
    {
        return $this->title . $this->date . $this->description;
    }

    public function dateStr(): string{
        $dateTime = new DateTime($this->date);
        return $dateTime->format('d/m/y à H:i') . '';
    }
}