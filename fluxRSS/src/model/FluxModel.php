<?php

namespace model;

use DAL\Connection;
use DAL\FluxGateway;
use metier\Flux;

class FluxModel
{
    public function findAllFlux(){
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $data = array();
        $result = $gateway->findAllFlux();

        foreach ($result as $row){
            $data[] = new Flux((int)$row['id'],$row['flux']);
        }
        return $data;
    }

    public function addFlux(Flux $flux){
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $data = $this->findFlux($flux);
        if ($data == array()) {
            $gateway->addFlux($flux);
        }
    }

    public function addFluxBySrc(string $flux): Flux {
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $newFlux = new Flux(null,$flux);
        $gateway->addFlux($newFlux);
        return $newFlux;
    }

    public function removeFlux(Flux $flux){
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $gateway->removeFlux($flux);
    }

    public function removeFluxById(int $id){
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $gateway->removeFluxById($id);
    }

    public function removeFluxBySrc(string $flux) {
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $gateway->removeFluxBySrc($flux);
    }

    public function findFlux(Flux $flux){
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $data = array();
        $result = $gateway->findFlux($flux);

        foreach ($result as $row){
            $data[] = new Flux((int)$row['id'],$row['$flux']);
        }
        return $data;
    }

    public function findFluxBySrc(string $flux){
        $gateway = new FluxGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        $data = array();
        $result = $gateway->findFluxBySrc($flux);

        foreach ($result as $row){
            $data[] = new Flux((int)$row['id'],$row['$flux']);
        }
        return $data;
    }
}