<?php

namespace model;

use DAL\ArticleGateway;
use DAL\Connection;
use DAL\FluxGateway;
use DateTime;
use DOMDocument;
use Exception;
use metier\Article;
use metier\Flux;
use Twig\Error\Error;

class Parser
{
    private FluxGateway $fluxGateway;
    private ArticleGateway $articleGateway;

    public function __construct(FluxGateway $fluxGateway, ArticleGateway $articleGateway){
        $this->fluxGateway = $fluxGateway;
        $this->articleGateway = $articleGateway;
    }
    public function parseArticles(Flux $flux): array
    {
        $dom = new DOMDocument();
        $tabArticle = array();
        try {
            if ($dom->load($flux->getFlux())) {
                var_dump($flux);
                $items = $dom->getElementsByTagName('item');
                foreach ($items as $item) {
                    $title = $item->getElementsByTagName('title')[0]->nodeValue;
                    $date = $item->getElementsByTagName('pubDate')[0]->nodeValue;
                    $guid = $item->getElementsByTagName('guid')[0]->nodeValue;
                    $link = $item->getElementsByTagName('link')[0]->nodeValue;
                    $description = $item->getElementsByTagName('description')[0]->nodeValue;
                    $mediaUrl = null;
                    $mediaElements = $item->getElementsByTagNameNS('http://search.yahoo.com/mrss/', 'content');

                    // Vérifier si un élément media:content existe
                    if ($mediaElements->length > 0) {
                        $media = $mediaElements->item(0);
                        $mediaUrl = $media->getAttribute('url');
                    }

                    var_dump($mediaUrl);

                    $dateTime = new DateTime($date);
                    $tabArticle[] = new Article(
                        (int)null,
                        $title,
                        $dateTime->format('d/m/y H:i') . '',
                        $description,
                        $guid,
                        $link,
                        ($mediaUrl !== null) ? (string)$mediaUrl : '',
                        $flux->getId());
                }
                return $tabArticle;
            } else {
                // En cas d'erreur lors du chargement du flux RSS, lever une exception
                throw new Exception("Erreur lors du chargement du flux RSS");
            }
        }catch (Error $e){
            echo $e->getMessage();
            return $tabArticle;
        }catch (Exception $e2){
            echo $e2->getMessage();
            return $tabArticle;
        }
    }


    /**
     * @throws Exception
     */
    public function parseAll($fluxes){
        $tabArticles = [];
        foreach ($fluxes as $flux){
            $fluxx = new Flux($flux[0],$flux[1]);
            $tabArticles[] = $this->parseArticles($fluxx);

        }
        return $tabArticles;
    }

    /**
     * @throws Exception
     */
    public function addAllArticles()
    {
        $tabFluxes = [];
        $this->articleGateway->removeAllArticleForParser();

        $allItemFlux = $this->fluxGateway->findAllFlux();
        foreach ($allItemFlux as $ItemFlux){
            $tabFluxes[] = new Flux(intval($ItemFlux['id']), $ItemFlux['flux']);
        }

        foreach ($tabFluxes as $flux) {
            $tabArticle = $this->parseArticles($flux);
            foreach ($tabArticle as $item) {
                $this->articleGateway->addArticle($item);
            }

        }
    }

}






