<?php

namespace model;

use config\Validation;
use DAL\AdminGateway;
use DAL\Connection;
use metier\Admin;

class AdminModel
{
    /**
     * @throws \Exception
     */
    public function connection (string $username, string $mdp){


        $gwArticle = new AdminGateway(new Connection('mysql:host=londres.uca.local;dbname=dbrorossetto','rorossetto','tpphp'));
        if (Validation::validationLogin($username)){
            $lmdp = $gwArticle->login($username);
        }
        if(Validation::validationMdp($mdp)){
            foreach ($lmdp as $motDePasse){
                if (password_verify($mdp,$motDePasse['password']) or $mdp == $motDePasse['password']){ //Si ajout d'admin on créer avec password_hash(string $mdp)
                    $_SESSION['role'] = 'admin';
                    $_SESSION['pseudo'] = $username;
                    return new Admin($username,$motDePasse['mail']);
                }
            }
        }
        return null;
    }

    public static function isAdmin(): bool
    {
        return (isset($_SESSION['role']) && $_SESSION['role'] == 'admin');
    }

    public static function deconnection(){
        $_SESSION['role'] = "";
        unset($_SESSION['role']);
        $_SESSION['pseudo'] = "";
        unset($_SESSION['pseudo']);
        header("Location: /~rorossetto/Php_RSS/fluxRSS/admin/");
    }
}