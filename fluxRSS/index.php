<?php

//chargement config
require_once __DIR__ . '/src/config/config.php';

require __DIR__ . '/vendor/autoload.php';

use controleur\FrontControleur;

//twig
$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig   = new \Twig\Environment($loader, [
    'cache' => false,
]);

$cont = new FrontControleur();


